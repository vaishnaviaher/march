const hapi = require('@hapi/hapi')

const mongoose = require('mongoose')

const Joi = require('joi')

const server = new hapi.server({
    host: 'localhost',
    port: 4000
})

//connnet to the mongodb
mongoose.connect("mongodb://localhost:27017/EmployeeDB",{useNewUrlParser:true})

const personModel = mongoose.model("employees",{
    firstName: String,
    lastName: String
})

server.route({
    method: 'POST',
    path: '/person',
    options:{
        validate :{
            payload : Joi.object({
                firstName : Joi.string().min(1).max(5).required(),
                lastName: Joi.string().min(1).max(6).required()
            })
        }
    },
    handler: async (request, h) =>{
        try{
            var person = new personModel(request.payload);
            await person.save();
            return "person data inserted";
        }
        catch(error){
            return h.response(error).code(500);
        }        
    }
});



server.route({
    method: 'GET',
    path: '/getPersonData',
    handler: async (request, h) =>{
        try{
            var personData = personModel.find();
            return personData
        }
        catch(error){
            return h.response(error).code(500);
        }
    }
});

server.route({
    method: 'PUT',
    path: '/updatePersonDataById/{id}',
    options: {
        validate :{
            payload: Joi.object({
                firstName: Joi.string().optional(),
                lastName: Joi.string().optional()
            })
        }
    },
    handler: async (request ,  h) =>{
        try{
            var person = await personModel.findByIdAndUpdate(request.params.id,request.payload,new personModel);
            //console.log(request.params.id)
            return h.response(person);
        }
        catch(error){
            return h.response(error).code(500);
        }
        
    }
});

server.route({
    method: 'DELETE',
    path: '/deletePersonDataById/{id}',
    handler: async(request , h)=>{
        try{
            await personModel.findByIdAndDelete(request.params.id);
            return "person data deleted"
        }
        catch(error){
            return h.response(error).code(500);
        }
        
    }
})

server.start();

